#include "config.h"
#include <WiFiNet.h>
#include <Sprocket.h>
#include <ESPAsyncWebServer.h>
#include <WebServerConfig.h>
#include <WebServerPlugin.h>
#include <WebConfigPlugin.h>
#include <WebApiPlugin.h>
#include <PixelPlugin.h>
#include <IrcPlugin.h>
#include <OtaTcpPlugin.cpp>
#include <MqttPlugin.h>
#include <RcSwitchPlugin.h>

WiFiNet *network;
Sprocket *sprocket;
WebServerPlugin *webServerPlugin;
WebConfigPlugin *webConfigPlugin;
WebApiPlugin *webApiPlugin;
PixelPlugin *pixelPlugin;
IrcPlugin *ircPlugin;
OtaTcpPlugin *otaTcpPlugin;
MqttPlugin *mqttPlugin;
RcSwitchPlugin *rcSwitchPlugin;

void serveWebConfig(AsyncWebServer *server)
{
    server->serveStatic(PIXEL_CONFIG_FILE, SPIFFS, "pixelConfig.json");
    server->serveStatic(IRC_CONFIG_FILE, SPIFFS, "ircConfig.json");
    server->serveStatic(MQTT_CONFIG_FILE, SPIFFS, "mqttConfig.json");
}

void subscribeChat(Sprocket *sprocket)
{
    sprocket->subscribe("irc/connected", [sprocket](String msg) {
        if (atoi(msg.c_str()))
        {
            sprocket->subscribe("irc/log", [sprocket](String msg) {
                PRINT_MSG(Serial, "CHAT", String("irc/log:  " + msg).c_str());
                sprocket->publish("ws/broadcast", msg);
            });
            sprocket->subscribe("out/chat/log", [sprocket](String msg) {
                PRINT_MSG(Serial, "CHAT", String("out/chat/log:  " + msg).c_str());
                sprocket->publish("irc/sendMessage", msg);
                sprocket->publish("ws/broadcast", "You:" + msg);
            });
            sprocket->publish("chat/connected", "");
        }
    });
}

void subscribeTardis(Sprocket *sprocket, NeoPattern *pixels)
{
    sprocket->subscribe("tardis/wibbly", [pixels](String msg) {
        pixels->Fade(0, pixels->Color(255, 255, 255), 4, 100, FORWARD);
    });
    sprocket->subscribe("tardis/wobbly", [pixels](String msg) {
        pixels->Scanner(pixels->Color(0, 255, 255), 100);
    });
    sprocket->subscribe("irc/connected", [sprocket](String msg) {
        sprocket->publish("tardis/wobbly", msg);
    });
}

void start(Sprocket *sprocket)
{
    sprocket->activate();
    sprocket->publish("tardis/wibbly", "");
    sprocket->publish("irc/connect", "");
}

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    pixelPlugin = new PixelPlugin({LED_STRIP_PIN, LED_STRIP_LENGTH, LED_STRIP_BRIGHTNESS, LED_STRIP_UPDATE_INTERVAL});
    ircPlugin = new IrcPlugin({IRC_SERVER, IRC_PORT, IRC_NICKNAME, IRC_USER});
    mqttPlugin = new MqttPlugin({MQTT_CLIENT_NAME, MQTT_HOST, MQTT_PORT, MQTT_ROOT_TOPIC});
    webServerPlugin = new WebServerPlugin({WEB_CONTEXT_PATH, WEB_DOC_ROOT, WEB_DEFAULT_FILE, WEB_PORT});
    webConfigPlugin = new WebConfigPlugin(webServerPlugin->server);
    webApiPlugin = new WebApiPlugin(webServerPlugin->server);
    otaTcpPlugin = new OtaTcpPlugin({OTA_PORT, OTA_PASSWORD});
    rcSwitchPlugin = new RcSwitchPlugin({PIN_RC_TX});
    network = new WiFiNet(WIFI_MODE, STATION_SSID, STATION_PASSWORD, AP_SSID, AP_PASSWORD, HOSTNAME, CONNECT_TIMEOUT);

    sprocket->addPlugin(pixelPlugin);
    sprocket->addPlugin(webServerPlugin);
    sprocket->addPlugin(webConfigPlugin);
    sprocket->addPlugin(webApiPlugin);
    sprocket->addPlugin(otaTcpPlugin);
    sprocket->addPlugin(ircPlugin);
    sprocket->addPlugin(rcSwitchPlugin);
    sprocket->addPlugin(mqttPlugin);
    network->connect();

    serveWebConfig(webServerPlugin->server);
    subscribeChat(sprocket);
    subscribeTardis(sprocket, pixelPlugin->pixels);
    start(sprocket);
    
}

void loop()
{
    sprocket->loop();
    yield();
}