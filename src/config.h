#ifndef __TARDIS_CONFIG__
#define __TARDIS_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip config
#define SPROCKET_TYPE       "ILLUCAT"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// Network config
#define WIFI_MODE           1
#define WIFI_CHANNEL        11
#define AP_SSID             "tardis"
#define AP_PASSWORD         "th3r31sn0sp00n"
#define STATION_SSID        "MyAP"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "tardis"
#define CONNECT_TIMEOUT     10000

// config files
#define PIXEL_CONFIG_FILE "/pixelConfig.json"
#define MQTT_CONFIG_FILE "/mqttConfig.json"
#define IRC_CONFIG_FILE "/ircConfig.json"

// NeoPixel
#define LED_STRIP_PIN D2
#define LED_STRIP_LENGTH 8
#define LED_STRIP_BRIGHTNESS 48
#define LED_STRIP_UPDATE_INTERVAL 200
#define LED_STRIP_DEFAULT_COLOR 100
#define COLOR_CONNECTED LED_STRIP_DEFAULT_COLOR
#define COLOR_NOT_CONNECTED 255

#define IRC_SERVER "chat.freenode.net"
#define IRC_PORT 6665
#define IRC_NICKNAME ""
#define IRC_USER ""

// OTA config
#define OTA_PORT 8266
#define OTA_PASSWORD ""

// WebServer
#define WEB_CONTEXT_PATH "/"
#define WEB_DOC_ROOT "/www"
#define WEB_DEFAULT_FILE "index.html"
#define WEB_PORT 80

// mqtt config
#define MQTT_CLIENT_NAME     "tardis"
#define MQTT_HOST            "192.168.1.2"
#define MQTT_PORT            1883
#define MQTT_ROOT_TOPIC      "wirelos/led/tardis"

// RC Switch
#define PIN_RC_TX                D8

#endif